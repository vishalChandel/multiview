package com.drandonadcolony.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;
import androidx.browser.customtabs.TrustedWebUtils;
import androidx.core.content.ContextCompat;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAdOptions;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;

import java.util.Collections;

import static androidx.browser.customtabs.CustomTabsClient.bindCustomTabsService;


public class MainActivity extends AppCompatActivity {
    WebView webView;
    private AdColonyInterstitial interstitialAdColony;
    private AdColonyInterstitialListener interstitialListener;
    private AdColonyAdOptions interstitialAdOptions;
    private static boolean isInterstitialLoaded;
    private final static String APP_ID = "app5ab619ff496942dcb9";
    private final static String INTERSTITIAL_ZONE_ID = "vzb8f1ff3b164349ca9d";
    private static final String TAG = "MainActivity";
    RelativeLayout relativeLayout;
    private static final int SESSION_ID = 96375;
    @Nullable
    private TwaCustomTabsServiceConnection mServiceConnection;

    @Nullable
    CustomTabsIntent mCustomTabsIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initColonySdk();
        initInterstitialAd();
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl("https://metropcs.mobileposse.com/metropcs/en/app-sports.html");
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {


                if (url.contains("https://www.msn.com/")) {
                    displayInterstitialAd(webView);
                    CustomTabsIntent.Builder customIntent = new CustomTabsIntent.Builder();
                    customIntent.setToolbarColor(ContextCompat.getColor(MainActivity.this, R.color.Blue));
                    openCustomTab(MainActivity.this, customIntent.build(), Uri.parse(url));
                    customIntent.setShowTitle(true);
                    customIntent.enableUrlBarHiding();
                    displayInterstitialAd(webView);


                    return true;
                } else if (url.contains("https://bleacherreport.com/")) {


                    displayInterstitialAd(webView);
                    TrustedWebUtils.launchAsTrustedWebActivity(
                            MainActivity.this,
                            mCustomTabsIntent,
                            Uri.parse(url));
                    displayInterstitialAd(webView);


                } else
                    displayInterstitialAd(webView);


                return false;
            }
        });
        String customTabsProviderPackage = CustomTabsClient.getPackageName(this, Collections.singletonList("com.android.chrome"), true);


        mServiceConnection = new TwaCustomTabsServiceConnection();
        bindCustomTabsService(
                this, customTabsProviderPackage, mServiceConnection);
    }


    public static void openCustomTab(Activity activity, CustomTabsIntent customTabsIntent, Uri uri) {
        // package name is the default package
        // for our custom chrome tab
        String packageName = "com.android.chrome";
        if (packageName != null) {

            // we are checking if the package name is not null
            // if package name is not null then we are calling
            // that custom chrome tab with intent by passing its
            // package name.
            customTabsIntent.intent.setPackage(packageName);

            // in that custom tab intent we are passing
            // our url which we have to browse.
            customTabsIntent.launchUrl(activity, uri);
        } else {
            // if the custom tabs fails to load then we are simply
            // redirecting our user to users device default browser.
            activity.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }
    }

    private class TwaCustomTabsServiceConnection extends CustomTabsServiceConnection {

        @SuppressLint("ResourceAsColor")
        @Override
        public void onCustomTabsServiceConnected(ComponentName componentName,
                                                 CustomTabsClient client) {

            // Creates a CustomTabsSession with a constant session id.
            CustomTabsSession session = client.newSession(null, SESSION_ID);

            // Creates a CustomTabsIntent to launch the Trusted Web Activity.
            mCustomTabsIntent = new CustomTabsIntent.Builder(session)
                    .setToolbarColor(Color.parseColor("#1ED526")).build();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "Twa CustomTab Service Disconnected");
        }
    }

    private void initColonySdk() {
        // Construct optional app options object to be sent with configure
//        AdColonyAppOptions appOptions = new AdColonyAppOptions().setKeepScreenOn(true);
//        ;
//        // Configure AdColony in your launching Activity's onCreate() method so that cached ads can
//        // be available as soon as possible.
//        AdColony.configure(getApplication(), appOptions, APP_ID, INTERSTITIAL_ZONE_ID);

        AdColonyAppOptions options = new AdColonyAppOptions().setOption("force_ad_id", "ad0f6d7804816046c8b1");
        AdColony.configure(getApplication(), options, APP_ID, INTERSTITIAL_ZONE_ID);
    }

    private void initInterstitialAd() {
        // Set up listener for interstitial ad callbacks. You only need to implement the callbacks
        // that you care about. The only required callback is onRequestFilled, as this is the only
        // way to get an ad object.
        interstitialListener = new AdColonyInterstitialListener() {
            @Override
            public void onRequestFilled(AdColonyInterstitial adIn) {
                // Ad passed back in request filled callback, ad can now be shown
                interstitialAdColony = adIn;
                isInterstitialLoaded = true;
            }

            @Override
            public void onRequestNotFilled(AdColonyZone zone) {
                super.onRequestNotFilled(zone);
            }

            @Override
            public void onOpened(AdColonyInterstitial ad) {
                super.onOpened(ad);
            }

            @Override
            public void onClosed(AdColonyInterstitial ad) {
                super.onClosed(ad);
                //request new Interstitial Ad on close
                AdColony.requestInterstitial(INTERSTITIAL_ZONE_ID, interstitialListener, interstitialAdOptions);
            }

            @Override
            public void onClicked(AdColonyInterstitial ad) {
                super.onClicked(ad);
            }

            @Override
            public void onLeftApplication(AdColonyInterstitial ad) {
                super.onLeftApplication(ad);
            }

            @Override
            public void onExpiring(AdColonyInterstitial ad) {
                super.onExpiring(ad);
            }
        };
        // Optional Ad specific options to be sent with request
        interstitialAdOptions = new AdColonyAdOptions();
        AdColony.requestInterstitial(INTERSTITIAL_ZONE_ID, interstitialListener, interstitialAdOptions);
    }

    public void displayInterstitialAd(View view) {
        if (interstitialAdColony != null && isInterstitialLoaded) {
            interstitialAdColony.show();
            isInterstitialLoaded = false;
        } else {
//            Toast.makeText(getApplicationContext(), "Interstitial Ad Is Not Loaded Yet or Request Not Filled", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {


        if (webView.canGoBack()) {
            webView.goBack();

        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mServiceConnection != null) {
            unbindService(mServiceConnection);
            webView.goBack();
        }
    }
}